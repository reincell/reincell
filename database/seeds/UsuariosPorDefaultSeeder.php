<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Persona;

class UsuariosPorDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Súper Dúper Administrador de Reincell';
        $user->email = 'super-duper-administrador@reincell.com';
        $user->password = bcrypt('12345678');
        $user->save();

        $persona = new Persona();
        $persona->user_id = 1;
        $persona->save();

        DB::table('role_user')->insert([
        	'role_id' => 1,
        	'user_id' => 1,
        	'estado' => 1,
        ]);


        /*
        DB::table('users')->insert([
        	'name' => 'Súper Dúper Administrador de Reincell',
        	'email' => 'super-duper-administrador@reincell.com',
        	'password' => bcrypt('12345678'),
        ]);

        DB::table('role_user')->insert([
        	'role_id' => 1,
        	'user_id' => 1,
        	'estado' => 1,
        ]);

        */


        DB::table('users')->insert([
        	'name' => 'Súper Administrador de Reincell',
        	'email' => 'super-administrador@reincell.com',
        	'password' => bcrypt('12345678'),
        ]);

        DB::table('role_user')->insert([
        	'role_id' => 2,
        	'user_id' => 2,
        	'estado' => 1,
        ]);

        $persona = new Persona();
        $persona->user_id = 2;
        $persona->save();

        DB::table('users')->insert([
        	'name' => 'Administrador de Clínica Reincell',
        	'email' => 'administrador@reincell.com',
        	'password' => bcrypt('12345678'),
        ]);

        DB::table('role_user')->insert([
        	'role_id' => 3,
        	'user_id' => 3,
        	'estado' => 1,
        ]);

        $persona = new Persona();
        $persona->user_id = 3;
        $persona->save();

        DB::table('users')->insert([
        	'name' => 'Doctor de Clínica Reincell',
        	'email' => 'doctor@reincell.com',
        	'password' => bcrypt('12345678'),
        ]);
        DB::table('role_user')->insert([
        	'role_id' => 4,
        	'user_id' => 4,
        	'estado' => 1,
        ]);

        $persona = new Persona();
        $persona->user_id = 4;
        $persona->save();

        DB::table('users')->insert([
        	'name' => 'Empleado de Clínica Reincell',
        	'email' => 'empleado@reincell.com',
        	'password' => bcrypt('12345678'),
        ]);

        DB::table('role_user')->insert([
        	'role_id' => 8,
        	'user_id' => 5,
        	'estado' => 1,
        ]);

        $persona = new Persona();
        $persona->user_id = 5;
        $persona->save();
    }
}
