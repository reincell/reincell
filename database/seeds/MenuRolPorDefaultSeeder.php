<?php

use Illuminate\Database\Seeder;

class MenuRolPorDefaultSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Menú # 1
        DB::table('menu')->insert([
        	'nombre' => 'Sidebar',
        	'url' => '#',
        	'icono' => 'list-ol',
        	'menu_id' => '0',
        	'orden' =>  '1',
        ]);

        //Menú # 2
        DB::table('menu')->insert([
        	'nombre' => 'Roles',
        	'url' => '#',
        	'icono' => 'user-tag',
        	'menu_id' => '0',
        	'orden' =>  '1',
        ]);

        //Menú # 3
        DB::table('menu')->insert([
        	'nombre' => 'Permisos',
        	'url' => '#',
        	'icono' => 'network-wired',
        	'menu_id' => '0',
        	'orden' =>  '1',
        ]);

        //Menú # 4
        DB::table('menu')->insert([
        	'nombre' => 'Menú Index',
        	'url' => 'admin/menu',
        	'icono' => 'list-ul',
        	'menu_id' => '1',
        	'orden' =>  '1',
        ]);

        //Menú # 5
        DB::table('menu')->insert([
        	'nombre' => 'Sidebar Dinámico',
        	'url' => 'admin/menu-rol',
        	'icono' => 'tasks',
        	'menu_id' => '1',
        	'orden' =>  '2',
        ]);

        //Menú # 6
        DB::table('menu')->insert([
        	'nombre' => 'Crear Menú',
        	'url' => 'admin/menu/crear',
        	'icono' => 'plus-circle',
        	'menu_id' => '1',
        	'orden' =>  '3',
        ]);

        //Menú # 7
        DB::table('menu')->insert([
        	'nombre' => 'Lista de roles',
        	'url' => 'admin/rol',
        	'icono' => 'list-ul',
        	'menu_id' => '2',
        	'orden' =>  '1',
        ]);

        //Menú # 8
        DB::table('menu')->insert([
        	'nombre' => 'Crear Rol',
        	'url' => 'admin/rol/crear',
        	'icono' => 'plus-circle',
        	'menu_id' => '2',
        	'orden' =>  '2',
        ]);

        //Menú # 9
        DB::table('menu')->insert([
        	'nombre' => 'Lista de permisos',
        	'url' => 'admin/permiso',
        	'icono' => 'list-ul',
        	'menu_id' => '3',
        	'orden' =>  '1',
        ]);

        //Menú # 10
        DB::table('menu')->insert([
        	'nombre' => 'Crear Permiso',
        	'url' => 'admin/permiso/crear',
        	'icono' => 'plus-circle',
        	'menu_id' => '3',
        	'orden' =>  '2',
        ]);


        ////Preconfiguración del super-duper-admin
        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '1',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '2',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '3',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '4',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '5',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '6',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '7',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '8',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '9',
        ]);

        DB::table('menu_rol')->insert([
        	'rol_id' => '1',
        	'menu_id' => '10',
        ]);
    }
}
