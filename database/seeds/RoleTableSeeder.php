<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role = new Role();
        $role->name = 'super-duper-admin';
        $role->description = 'Súper Dúper Administrator';
        $role->save();


    	$role = new Role();
        $role->name = 'super-admin';
        $role->description = 'Súper Administrator';
        $role->save();


        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();

        $role = new Role();
        $role->name = 'doctor';
        $role->description = 'Doctor';
        $role->save();

        $role = new Role();
        $role->name = 'secretaria';
        $role->description = 'Secretaria';
        $role->save();	

        $role = new Role();
        $role->name = 'proveedor';
        $role->description = 'Proveedor';
        $role->save();

        $role = new Role();
        $role->name = 'publicitor';
        $role->description = 'Agente de Publicidad';
        $role->save();

        $role = new Role();
        $role->name = 'empleado';
        $role->description = 'Empleado';
        $role->save();
    }
}
