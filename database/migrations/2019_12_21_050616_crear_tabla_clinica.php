<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaClinica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinica', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('telefono')->nullable();
            $table->unsignedInteger('tipo')->default(1);
            //dirección
            $table->string('domicilio')->default(' ');
            $table->string('colonia')->default(' ');
            $table->string('ciudad')->default(' ');
            $table->string('pais')->default('México');

            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_spanish_ci';
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('clinica');
        Schema::enableForeignKeyConstraints();
    }
}
