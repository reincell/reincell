<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
})->name('inicio');

Route::get('/home', 'HomeController@index');

Route::get('seguridad/login', 'Seguridad\LoginController@index')->name('login');
Route::post('seguridad/login', 'Seguridad\LoginController@login')->name('login_post');
Route::get('seguridad/logout', 'Seguridad\LoginController@logout')->name('logout');

Route::group(['prefix'=>'admin', 'namespace'=> 'Admin', 'middleware' => ['auth', 'super-duper-admin']], function (){
	Route::get('', 'AdminController@index');

	//Rutas del Permiso
	Route::get('permiso', 'PermisoController@index')->name('permiso');
	Route::get('permiso/crear', 'PermisoController@crear')->name('crear_permiso');
	Route::post('permiso', 'PermisoController@guardar')->name('guardar_permiso');
	Route::get('permiso/{id}/editar', 'PermisoController@editar')->name('editar_permiso');
	Route::put('permiso/{id}', 'PermisoController@actualizar')->name('actualizar_permiso');
	Route::delete('permiso/{id}', 'PermisoController@eliminar')->name('eliminar_permiso');

	//Rutas del Menú
	Route::get('menu', 'MenuController@index')->name('menu');
	Route::get('menu/crear', 'MenuController@crear')->name('crear_menu');
	Route::post('menu', 'MenuController@guardar')->name('guardar_menu');
	Route::get('menu/{id}/editar', 'MenuController@editar')->name('editar_menu');
	Route::put('menu/{id}', 'MenuController@actualizar')->name('actualizar_menu');
	Route::post('menu/guardar-orden', 'MenuController@guardarorden')->name('guardar-orden');
	Route::get('menu/{id}/eliminar', 'MenuController@eliminar')->name('eliminar_menu');

	//Rutas del eMail
	Route::get('email', 'UserRegisterController@create');
    Route::post('emails', 'UserRegisterController@store')->name('mails_data');

    //Rutas del Rol
    Route::get('rol', 'RolController@index')->name('rol');
	Route::get('rol/crear', 'RolController@crear')->name('crear_rol');
	Route::post('rol', 'RolController@guardar')->name('guardar_rol');
	Route::get('rol/{id}/editar', 'RolController@editar')->name('editar_rol');
	Route::put('rol/{id}', 'RolController@actualizar')->name('actualizar_rol');
	//Route::delete('rol/{id}', 'RolController@eliminar')->name('eliminar_rol');
	Route::delete('rol/eliminar', 'RolController@eliminar')->name('eliminar_rol');

	//Rutas del Menu-Rol
	Route::get('menu-rol', 'MenuRolController@index')->name('menu_rol');
	Route::post('menu-rol', 'MenuRolController@guardar')->name('guardar_menu_rol');

	//Rutas del Permiso-Rol
	Route::get('permiso-rol', 'PermisoRolController@index')->name('permiso_rol');
	Route::post('permiso-rol', 'PermisoRolController@guardar')->name('guardar_permiso_rol');

    //perfil
    Route::get('perfil', 'PersonaController@perfil');
    Route::get('actualizar', 'PersonaController@index')->name('persona_data');
    Route::post('actualizar/datos_personales', 'PersonaController@update')->name('persona_data');
});

Route::group(['middleware' => ['auth']], function (){

    //perfil
    Route::get('perfil', 'PersonaController@perfil');
    Route::get('actualizar', 'PersonaController@index')->name('persona_data');
    Route::post('actualizar/datos_personales', 'PersonaController@update')->name('persona_data');
});
