$(document).ready( function(){
	$("#tabla-data").on('submit', '.form-eliminar', function () {
		event.preventDefault();
        const form = $(this);
        swal({
			title: '¿Está seguro de que desea elminar este registro?',
			text: "Esta acción no se puede deshacer!",
			icon: 'warning',
			buttons: {
				cancel: "Cancelar",
				confirm: "Aceptar"
			},
        }).then((value) => {
            if (value) {
                ajaxRequest(form);
            }
        });
    });

	function ajaxRequest(form) {
        $.ajax({
            url: form.attr('action'),
            //type: 'POST',
            type: 'DELETE',
            data: form.serialize(),
            success: function (respuesta) {
                if (respuesta.mensaje == "ok") {
                    form.parents('tr').remove();
					Reincell.notificaciones('El registro ha sido eliminado de forma correcta', 'Reincell', 'success');
				}
				else {
					Reincell.notificaciones('El registro no puede ser eliminado, hay recursos utilizandolo como referencia', 'Reincell', 'error');
				}
			},

			error: function(respuesta){
				if(respuesta.responseJSON.message.includes("SQLSTATE[23000]: Integrity constraint violation")){
					Reincell.notificaciones('El registro no puede ser eliminado, hay recursos utilizandolo como referencia', 'Reincell', 'error');
				}
				else{
					Reincell.notificaciones('Error en Ajax', 'Reincell', 'error');
				}
				
			}
		});
	}
});