<!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- Cabecera -->
    @include('tema.secciones.librerias')
    <body>
        <div class="main-wrapper">
            <!-- Header -->
            @include('tema.secciones.header')

            <!-- SideBar -->
            @include('tema.secciones.sidebar')

            <!-- Conenido -->
            <div class="page-wrapper">
                <seccion class="content">
                    @yield('contenido')
                </seccion> <!-- Div Content -->
            </div><!-- Div page-wrapper -->
        </div>
        <div class="sidebar-overlay" data-reff=""></div>
        <!-- Scripts -->
        @include('tema.secciones.scripts')
    </body>
    
</html>





