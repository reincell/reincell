<!DOCTYPE html>
<html lang="es">

<head>
    @include('tema.secciones.librerias')
</head>

<body>
    <div class="main-wrapper account-wrapper">
        <div class="account-page">
			<div class="account-center">
				<div class="account-box">
                    
                    <form action="{{route('login_post')}}" class="form-signin" method="POST" autocomplete="off">
                        @csrf
						<div class="account-logo">
                            <a href="{{route('inicio')}}"><img src="{!! asset('assets/img/logo_base.svg') !!}">
                                <span class="float-center"><h4 style="color:#5AC1B8;">R E I N C E L L</h4></span>
                            </a>
                            
                        </div>
                        <div>
                            @if($errors->any())
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Correo Eléctronico</label>
                            <input type="text" autofocus="" class="form-control" name="email" value="{{old('email')}}">
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary account-btn">Ingresar al sistema</button>
                        </div>
                    </form>
                </div>
			</div>
        </div>
    </div>
    @include('tema.secciones.scripts')
</body>


<!-- Mirrored from dreamguys.co.in/preclinic/template/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 14 Aug 2019 16:12:01 GMT -->
</html>