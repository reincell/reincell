@if( session("mensaje") )
	<div class="alert alert-success alert-dismissible fade show" role="alert" data-auto-dismiss="3000">
		<h4><strong>Correcto!</strong> </h4>
		<h5>Mensaje del Sistema Reincell</h5>
		<ul>
			<li>{{ session("mensaje") }}</li>
		</ul>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
	</div>
@endif