@if($errors->any())
	<div class="alert alert-danger alert-dismissible fade show" role="alert">
		<h4><strong>Advertencia!</strong></h4> 
		<h4>El formulario contiene errores</h4>
		<ul>
			@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
		</ul>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
	</div>
@endif