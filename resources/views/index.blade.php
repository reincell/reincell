<!DOCTYPE html>
    <html>
    <!-- Cabecera -->
    @include('tema.secciones.librerias')
    <body>
        <div class="main-wrapper">
            <!-- Header -->
            @include('tema.secciones.header')

            <!-- SideBar -->
            @include('tema.secciones.sidebar')

            <!-- Conenido -->
            <div class="page-wrapper">
                <div class="content">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">Tablero</div>

                                <div class="card-body">

                                    @include('includes.mensaje')


                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- Div Content -->
            </div><!-- Div page-wrapper -->
        </div>
        <div class="sidebar-overlay" data-reff=""></div>
        <!-- Scripts -->
        @include('tema.secciones.scripts')
    </body>
    
</html>





