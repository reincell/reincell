@extends('base')

@section('scripts')
<script src="{!! asset('assets/pages/admin/permiso-rol/index.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
	<div class="row">
		<div class="col-lg-12">
			@include('includes.mensaje')
			<div class="card-box">
		        <div class="card-block">
		            <h5 class="text-bold card-title">Permiso - Rol</h5>
					@csrf
					<div class="table-responsive">
						<table class="table table-hover col-lg-12" id="tabla-data">
							<thead>
								<tr>
									<th>Permiso</th>
									@foreach($rols as $id => $nombre)
										<th> {{$nombre}} </th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								@foreach($permisos as $key => $permiso)
									<tr>
										<td class="font-weight-bold">{{ $permiso["nombre"] }}</td>
										@foreach($rols as $id => $nombre)
											<td class="text-center">
												<input 
													type="checkbox" 
													class="permiso_rol" 
													name="permiso_rol[]"
													data-permisoid= {{$permiso["id"]}}
													value="{{$id}}"
													{{in_array($id, array_column($permisosRols[$permiso["id"]], "id" ))? "checked" : "" }} 
													>
											</td>
										@endforeach		
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
		        </div>
		    </div>
		</div>
	</div>
@endsection