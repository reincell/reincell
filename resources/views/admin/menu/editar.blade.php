@extends('base')

@section('scripts')
<script src="{!! asset('js/crear.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
<div class="col-lg-12">
	@include('includes.form-error')
	@include('includes.mensaje')
        
    <div class="card-box">
        <a href="{{ route('menu') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-undo"></i> Volver al listado</a>
        <br>
        <div class="card-block">
            <h5 class="text-bold card-title">Editar Menú</h5>
			<form action="{{ route('actualizar_menu', ['id' => $data->id]) }}" id="form-general" method="POST" autocomplete="off">
				@csrf @method('put')
				@include('admin.menu.form')
                <div class=" text-center">
                	@include('includes.boton-editar-form')
                </div>
            </form>
        </div>
    </div>
</div>
@endsection