@extends('base')

@section('styles')
<link rel="stylesheet" href="{!! asset('js/jquery-nestable/jquery.nestable.css') !!} ">
@endsection

@section('scriptsPlugins')
<script src="{!! asset('js/jquery-nestable/jquery.min.js') !!} " type="text/javascript"></script>
<script src="{!! asset('js/jquery-nestable/jquery.nestable.js') !!} " type="text/javascript"></script>
@endsection

@section('scripts')
<script src="{!! asset('js/index.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
	<div class="row">
		<div class="col-lg-12">
			@include('includes.mensaje')
			<div class="card-box">
		        <div class="card-block">
		        	<a href="{{ route('crear_menu') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Nuevo Menú</a>
		            <h5 class="text-bold card-title">Menús</h5>
					@csrf
					<div class="cf nestable-lists">
						<div class="dd" id="nestable">
							<ol class="dd-list">
								@foreach ($menus as $key => $item)
									@if($item['menu_id'] != 0)
										@break
									@endif
									@include('admin.menu.menu-item', ['item' => $item])
								@endforeach
							</ol>
						</div>
					</div>
		        </div>
		    </div>
		</div>
	</div>
@endsection