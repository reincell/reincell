@extends('base')

@section('scripts')
<script src="{!! asset('assets/pages/admin/menu-rol/index.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
	<div class="row">
		<div class="col-lg-12">
			@include('includes.mensaje')
			<div class="card-box">
		        <div class="card-block">
		        	<a href="{{ route('crear_rol') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Nuevo Rol</a>
		            <h5 class="text-bold card-title">Menú - Rol</h5>
					@csrf
					<div class="table-responsive">
						<table class="table table-hover col-lg-12" id="tabla-data">
							<thead>
								<tr>
									<th>Menú</th>
									@foreach($roles as $id => $nombre)
										<th> {{$nombre}} </th>
									@endforeach
								</tr>
							</thead>
							<tbody>
								@foreach($menus as $key => $menu)
									@if($menu["menu_id"] != 0)
										@break
									@endif

									<tr>
										<td class="font-weight-bold"><i class="fa fa-expand-arrows-alt"></i> <i style="font-size:20px;" class="fa fa-{{isset($menu['icono']) ? $menu['icono'] : ''}} "></i> {{ $menu["nombre"] }}</td>
										@foreach($roles as $id => $nombre)
											<td class="text-center">
												<input 
													type="checkbox" 
													class="menu_rol" 
													name="menu_rol[]"
													data-menuid= {{$menu["id"]}}
													value="{{$id}}"
													{{in_array($id, array_column($menusRoles[$menu["id"]], "id" ))? "checked" : "" }} 
													>
											</td>
										@endforeach		
									</tr>
									@foreach($menu["submenu"] as $key =>$hijo)
										<tr>
											<td class="pl-30"><i class="fa fa-long-arrow-alt-right"></i> <i style="font-size:20px;" class="fa fa-{{isset($hijo['icono']) ? $hijo['icono'] : ''}} "></i> 
												{{$hijo["nombre"]}}
											</td>
											@foreach($roles as $id => $nombre)
												<td class="text-center">
													<input 
													type="checkbox" 
													class="menu_rol" 
													name="menu_rol[]" 
													data-menuid= {{$hijo["id"]}} 
													value="{{$id}}" 
													{{in_array($id, array_column($menusRoles[$hijo["id"]], "id" ))? "checked" : "" }}
													>
												</td>
											@endforeach
										</tr>
										@foreach($hijo["submenu"] as $key =>$hijo2)
											<tr>
												<td class="pl-40"><i class="fa fa-long-arrow-alt-right"></i> <i style="font-size:20px;" class="fa fa-{{isset($hijo2['icono']) ? $hijo2['icono'] : ''}} "></i> 
													{{$hijo2["nombre"]}}
												</td>
												@foreach($roles as $id => $nombre)
													<td class="text-center">
														<input 
														type="checkbox" 
														class="menu_rol" 
														name="menu_rol[]"
														data-menuid= {{$hijo2["id"]}}
														value="{{$id}}" 
														{{in_array($id, array_column($menusRoles[$hijo2["id"]], "id" ))? "checked" : "" }}
														>
													</td>
												@endforeach
											</tr>
											@foreach($hijo2["submenu"] as $key =>$hijo3)
												<tr>
													<td class="pl-50"><i class="fa fa-long-arrow-alt-right"></i>  <i style="font-size:20px;" class="fa fa-{{isset($hijo3['icono']) ? $hijo3['icono'] : ''}} "></i>
														{{$hijo3["nombre"]}}
													</td>
													@foreach($roles as $id => $nombre)
														<td class="text-center">
															<input 
															type="checkbox" 
															class="menu_rol" 
															name="menu_rol[]"
															data-menuid= {{$hijo3["id"]}}
															value="{{$id}}" 
															{{in_array($id, array_column($menusRoles[$hijo3["id"]], "id" ))? "checked" : "" }}
															>
														</td>
													@endforeach
												</tr>
												@foreach($hijo3["submenu"] as $key =>$hijo4)
													<tr>
														<td class="pl-60"><i class="fa fa-long-arrow-alt-right"></i> <i style="font-size:20px;" class="fa fa-{{isset($hijo4['icono']) ? $hijo4['icono'] : ''}} "></i>
															{{$hijo4["nombre"]}}
														</td>
														@foreach($roles as $id => $nombre)
															<td class="text-center">
																<input 
																type="checkbox" 
																class="menu_rol" 
																name="menu_rol[]"
																data-menuid= {{$hijo4["id"]}}
																value="{{$id}}" 
																{{in_array($id, array_column($menusRoles[$hijo4["id"]], "id" ))? "checked" : "" }}
																>
															</td>
														@endforeach
													</tr>
												@endforeach
											@endforeach
										@endforeach
									@endforeach
								@endforeach

							</tbody>
						</table>
					</div>
		        </div>
		    </div>
		</div>
	</div>
@endsection