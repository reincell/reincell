@extends('base')

@section('scripts')
<script src="{!! asset('assets/pages/admin/index.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
	<div class="row">
		<div class="col-lg-12">
			@include('includes.mensaje')
			<div class="card-box">
		        <div class="card-block">
		        	<a href="{{ route('crear_rol') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Nuevo Rol</a>
		            <h5 class="text-bold card-title">Roles</h5>
					
					<div class="table-responsive">
						<table class="table table-hover col-lg-12" id="tabla-data">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Descripción</th>
									<th class="width70">Acción</th>
								</tr>
							</thead>
							<tbody>
								@foreach($datas as $data)
									<tr>
										<td> {{$data->name}} </td>
										<td> {{$data->description}} </td>
										<td> 
											<a href=" {{route('editar_rol', ['id' => $data->id]) }}" class="btn-accion-tabla tooltipsC" title="Editar este registro">
												<i class="fa fa-edit"></i>
											</a>
											
											<!--form action=" {{ route('eliminar_rol', ['id' => $data->id]) }}" class="d-inline form-eliminar" method="POST"-->
											<form action=" {{ route('eliminar_rol')}}" class="d-inline form-eliminar" method="POST">
												@csrf @method('delete')
												<input type="hidden" name="id" value="{{$data->id}}">
												<button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro">
													<i class="fa fa-trash text-danger"></i>
												</button>
											</form>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
		        </div>
		    </div>
		</div>
	</div>
@endsection