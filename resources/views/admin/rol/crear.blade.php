@extends('base')

@section('scripts')
<script src="{!! asset('js/crear.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
<div class="col-lg-12">
	@include('includes.form-error')
	@include('includes.mensaje')
    <div class="card-box">
        <a href="{{ route('rol') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-undo"></i> Volver al listado</a>
        <div class="card-block">
            <h5 class="text-bold card-title">Crear Rol</h5>
			<form action="{{ route('guardar_rol') }}" id="form-general" method="POST" autocomplete="off">
				@csrf
				@include('admin.rol.form')
                <div class=" text-center">
                	@include('includes.boton-crear-form')
                </div>
            </form>
        </div>
    </div>
</div>
@endsection