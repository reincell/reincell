<div class="form-group row">
    <label for="name" class="col-lg-2 col-form-label requerido">Nombre</label>
    <div class="col-lg-9">
        <input type="text" name="name" id="name" class="form-control" value="{{old('name', $data->name ?? '')}}" required />
    </div>
    <label for="description" class="col-lg-2 col-form-label requerido">Descripción</label>
    <div class="col-lg-9">
        <input type="text" name="description" id="description" class="form-control" value="{{old('description', $data->description ?? '')}}" required />
    </div>
</div>
</div>