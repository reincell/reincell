@extends('base')

@section('scripts')
<script src="{!! asset('js/crear.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
<div class="col-lg-12">
	@include('includes.form-error')
	@include('includes.mensaje')
    <div class="card-box">
        <a href="{{ route('rol') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-undo"></i> Volver al listado</a>
        <div class="card-block">
            <h5 class="text-bold card-title">Editar Rol</h5>
			<form action="{{ route('actualizar_rol', ['id' => $data->id]) }}" id="form-general" method="POST" autocomplete="off">
				@csrf @method('put')
				@include('admin.rol.form')
                <div class=" text-center">
                	@include('includes.boton-editar-form')
                </div>
            </form>
        </div>
    </div>
</div>
@endsection