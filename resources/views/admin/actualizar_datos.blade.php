<!DOCTYPE html>
    <html>
    <!-- Cabecera -->
    @include('tema.secciones.librerias')
    <body>
        <div class="main-wrapper">
            <!-- Header -->
            @include('tema.secciones.header')

            <!-- SideBar -->
            @include('tema.secciones.sidebar')

            <!-- Conenido -->
            <div class="page-wrapper">
                <div class="content">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            @include('includes.form-error')
                            @include('includes.mensaje')
                            <div class="card">
                                <div class="card-header">Registrar correo nuevo</div>
                                <!--    Formulario        -->
                                <div class="card-body">
                                    <div class="main-wrapper  account-wrapper">
                                        <div class="account-page">
                                            <div class="account-center">
                                                <div class="account-box" style="border-color: black; width: 100%">
                                                    <form form action="{{ route('persona_data') }}" method="post" class="form-signin">
                                                        {{ csrf_field() }}
                                                        <div class="form-row">
                                                            <div class="form-group col-md-4">
                                                                <label for="inputEmail4">Nombre(s)</label>
                                                                <input name="nombres" type="text" class="form-control" placeholder="Nombres" required>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputPassword4">Apellido paterno</label>
                                                                <input name="apellido_paterno" type="text" class="form-control" placeholder="Apellido" required>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputPassword4">Apellido materno</label>
                                                                <input name="apellido_materno" type="text" class="form-control" placeholder="Apellido" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label for="inputEmail4">CURP</label>
                                                                <input name="curp" type="text" class="form-control" placeholder="CURP" required>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputPassword4">Télefono</label>
                                                                <input name="telefono" type="text" class="form-control" placeholder="Teléfono" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword4">Dirección</label>
                                                            <input name="direccion" type="text" class="form-control" placeholder="Dirección" required>
                                                        </div>

                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label for="inputEmail4">Colonia</label>
                                                                <input name="colonia" type="text" class="form-control" placeholder="Colonia" required>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputZip">Código postal</label>
                                                                <!-- <input  type="text" class="form-control" > -->
                                                                <input name="codigo_postal" type="text" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-4">
                                                                <label for="inputCity">País</label>
                                                                <select name="pais" class="form-control">
                                                                    <option value="México" selected>México</option>
                                                                    <option>...</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputState">Ciudad</label>
                                                                <select name="ciudad" class="form-control">
                                                                    <option value="Guadalajara" selected>Guadalajara</option>
                                                                    <option>...</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputState">Estado</label>
                                                                <select name="estado" class="form-control">
                                                                    <option value="Jalisco" selected>Jalisco</option>
                                                                    <option>...</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="form-check">
                                                                <input class="form-check-input" type="checkbox" id="checkbox" required>
                                                                <label class="form-check-label" for="gridCheck">
                                                                    Check me out
                                                                </label>
                                                            </div>
                                                        </div>
                                                        @include('includes.boton-editar-form')
                                                    </form>
                                                    <!--
                                                    <form action="{{ route('mails_data') }}" method="post" class="form-signin">
                                                        {{ csrf_field() }}
                                                        <div class="account-logo">
                                                            <a href="index.html"><img src="{!! asset('assets/img/logo.png') !!}" alt=""></a>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label>Usuario</label>
                                                                <input type="text" name="name" id="username_form" class="form-control" oninput="ValidateForm()" required>
                                                                <label id="Label_Valid_Username">

                                                                </label>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputState">Privilegios</label>
                                                                <select name="rol" id="Roles_form" class="form-control" required>
                                                                    <option selected value="8">Empleado</option>
                                                                    <option value="4">Doctor</option>
                                                                    <option value="5">Secretaria</option>
                                                                    <option value="6">Proveedor</option>
                                                                    <option value="7">Publicista</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Contraseña</label>
                                                            <input type="password" id="Password1"  class="form-control" oninput="ValidateForm()" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Confirmar contraseña</label>
                                                            <input type="password" name="password" id="Password2" oninput="ValidateForm()" class="form-control" required>
                                                        </div>
                                                        <div class="">
                                                            <label id="Label_Valid_Pwd">

                                                            </label>
                                                        </div>
                                                        <div class="form-group checkbox">
                                                            <label>
                                                                <input type="checkbox" required > I have read and agree the Terms & Conditions
                                                            </label>
                                                        </div>
                                                        <div class="form-group text-center">
                                                            <button id="Submit_btn" class="btn btn-primary account-btn" type="submit" disabled>Register</button>
                                                        </div>
                                                    </form>
                                                -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--    Fin Formulario        -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- Div Content -->
            </div><!-- Div page-wrapper -->
        </div>
        <div class="sidebar-overlay" data-reff=""></div>
        <!-- Scripts -->
        @include('tema.secciones.scripts')
    </body>

</html>
