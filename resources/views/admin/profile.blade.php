@extends('base')

@section('styles')
<!--link rel="stylesheet" href="{!! asset('js/jquery-nestable/jquery.nestable.css') !!} "-->
@endsection

@section('scriptsPlugins')
<!--script src="{!! asset('js/jquery-nestable/jquery.min.js') !!} " type="text/javascript"></script>
<script src="{!! asset('js/jquery-nestable/jquery.nestable.js') !!} " type="text/javascript"></script-->
@endsection

@section('scripts')
<!--script src="{!! asset('js/index.js') !!} " type="text/javascript"></script-->
@endsection

@section('contenido')
<div class="row">
	<div class="col-sm-7 col-6">
		<h4 class="page-title">My Profile</h4>
	</div>
	<div class="col-sm-5 col-6 text-right m-b-30">
		<a href="edit-profile.html" class="btn btn-primary btn-rounded"><i class="fa fa-plus"></i> Edit Profile</a>
	</div>
</div>
<div class="card-box profile-header">
	<div class="row">
		<div class="col-md-12">
			<div class="profile-view">
				<div class="profile-img-wrap">
					<div class="profile-img">
						<a href="#"><img class="avatar" src="assets/img/doctor-03.jpg" alt=""></a>
					</div>
				</div>
				<div class="profile-basic">
					<div class="row">
						<div class="col-md-5">
							<div class="profile-info-left">
								<h3 class="user-name m-t-0 mb-0">{{ $persona->nombres }}</h3>
								<small class="text-muted"> {{ $persona->curp }}</small>
								<div class="staff-id">Employee ID : DR-0001</div>
								<div class="staff-msg"><a href="chat.html" class="btn btn-primary">Send Message</a></div>
							</div>
						</div>
						<div class="col-md-7">
							<ul class="personal-info">
								<li>
									<span class="title">Teléfono:</span>
									<span class="text"><a href="#">{{ $persona->telefono }}</a></span>
								</li>
								<li>
									<span class="title">Email:</span>
									<span class="text"><a href="#">{{ $persona->email }}</a></span>
									<!--
									<span class="text"><a href="#"><span class="__cf_email__" data-cfemail="c4a7b6adb7b0adaaa5a3b6abb2a1b784a1bca5a9b4a8a1eaa7aba9">[email&#160;protected]</span></a></span>
									-->

								</li>
								<li>
									<span class="title">Cumpleaños:</span>
									<span class="text">3rd March</span>
								</li>
								<li>
									<span class="title">Domicilio:</span>
									<span class="text">{{ $persona->pais.', '.$persona->ciudad.', '.$persona->estado  }}</span>
								</li>
								<li>
									<span class="title">Genéro:</span>
									<span class="text">Undefined</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="profile-tabs">
	<ul class="nav nav-tabs nav-tabs-bottom">
		<li class="nav-item"><a class="nav-link active" href="#about-cont" data-toggle="tab">About</a></li>
		<li class="nav-item"><a class="nav-link" href="#bottom-tab2" data-toggle="tab">Profile</a></li>
		<li class="nav-item"><a class="nav-link" href="#bottom-tab3" data-toggle="tab">Messages</a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane show active" id="about-cont">
			<div class="row">
				<div class="col-md-12">
					<div class="card-box">
						<h3 class="card-title">Education Informations</h3>
						<div class="experience-box">
							<ul class="experience-list">
								<li>
									<div class="experience-user">
										<div class="before-circle"></div>
									</div>
									<div class="experience-content">
										<div class="timeline-content">
											<a href="#/" class="name">International College of Medical Science (UG)</a>
											<div>MBBS</div>
											<span class="time">2001 - 2003</span>
										</div>
									</div>
								</li>
								<li>
									<div class="experience-user">
										<div class="before-circle"></div>
									</div>
									<div class="experience-content">
										<div class="timeline-content">
											<a href="#/" class="name">International College of Medical Science (PG)</a>
											<div>MD - Obstetrics & Gynaecology</div>
											<span class="time">1997 - 2001</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="card-box mb-0">
						<h3 class="card-title">Experience</h3>
						<div class="experience-box">
							<ul class="experience-list">
								<li>
									<div class="experience-user">
										<div class="before-circle"></div>
									</div>
									<div class="experience-content">
										<div class="timeline-content">
											<a href="#/" class="name">Consultant Gynecologist</a>
											<span class="time">Jan 2014 - Present (4 years 8 months)</span>
										</div>
									</div>
								</li>
								<li>
									<div class="experience-user">
										<div class="before-circle"></div>
									</div>
									<div class="experience-content">
										<div class="timeline-content">
											<a href="#/" class="name">Consultant Gynecologist</a>
											<span class="time">Jan 2009 - Present (6 years 1 month)</span>
										</div>
									</div>
								</li>
								<li>
									<div class="experience-user">
										<div class="before-circle"></div>
									</div>
									<div class="experience-content">
										<div class="timeline-content">
											<a href="#/" class="name">Consultant Gynecologist</a>
											<span class="time">Jan 2004 - Present (5 years 2 months)</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="bottom-tab2">
			Tab content 2
		</div>
		<div class="tab-pane" id="bottom-tab3">
			Tab content 3
		</div>
	</div>
</div>
@endsection
