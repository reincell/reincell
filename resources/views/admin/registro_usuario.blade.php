<!DOCTYPE html>
    <html>
    <!-- Cabecera -->
    @include('tema.secciones.librerias')
    <body>
        <div class="main-wrapper">
            <!-- Header -->
            @include('tema.secciones.header')

            <!-- SideBar -->
            @include('tema.secciones.sidebar')

            <!-- Conenido -->
            <div class="page-wrapper">
                <div class="content">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            @include('includes.form-error')
                            @include('includes.mensaje')
                            <div class="card">
                                <div class="card-header">Registrar correo nuevo</div>
                                <!--    Formulario        -->
                                <div class="card-body">
                                    <div class="main-wrapper  account-wrapper">
                                        <div class="account-page">
                                            <div class="account-center">
                                                <div class="account-box" style="border-color: black; width: 100%">
                                                    <form action="{{ route('mails_data') }}" method="post" class="form-signin">
                                                        {{ csrf_field() }}
                                                        <div class="account-logo">
                                                            <a href="index.html"><img src="{!! asset('assets/img/logo.png') !!}" alt=""></a>
                                                        </div>
                                                        <div class="form-row">
                                                            <div class="form-group col-md-8">
                                                                <label>Usuario</label>
                                                                <input type="text" name="name" id="username_form" class="form-control" oninput="ValidateForm()" required>
                                                                <label id="Label_Valid_Username">

                                                                </label>
                                                            </div>
                                                            <div class="form-group col-md-4">
                                                                <label for="inputState">Privilegios</label>
                                                                <select name="rol" id="Roles_form" class="form-control" required>
                                                                    <option selected value="8">Empleado</option>
                                                                    <option value="4">Doctor</option>
                                                                    <option value="5">Secretaria</option>
                                                                    <option value="6">Proveedor</option>
                                                                    <option value="7">Publicista</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Contraseña</label>
                                                            <input type="password" id="Password1"  class="form-control" oninput="ValidateForm()" required>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Confirmar contraseña</label>
                                                            <input type="password" name="password" id="Password2" oninput="ValidateForm()" class="form-control" required>
                                                        </div>
                                                        <div class="">
                                                            <label id="Label_Valid_Pwd">

                                                            </label>
                                                        </div>
                                                        <div class="form-group checkbox">
                                                            <label>
                                                                <input type="checkbox" required > I have read and agree the Terms & Conditions
                                                            </label>
                                                        </div>
                                                        <div class="form-group text-center">
                                                            <button id="Submit_btn" class="btn btn-primary account-btn" type="submit" disabled>Register</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--    Fin Formulario        -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- Div Content -->
            </div><!-- Div page-wrapper -->
        </div>
        <div class="sidebar-overlay" data-reff=""></div>
        <!-- Scripts -->
        <script>
            var Var_Invalid_Pwd = "La contraseña debe ser alfanumerica y tener al menos 8 caracteres y una mayúscula";
            var Var_Valid_Pwd = "Las contraseñas coinciden";
            var Var_Invalid_Pwd2 = "Las contraseñas no coinciden";
            var Var_Invalid_Username = "El nombre de usuario no es válido";

            var caracteres = /^[a-zA-Z0-9]+$/;
            var secure_password = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
            var valid_name = /^(?=.{5,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/;

            function ValidatePasswords()
            {
                if(secure_password.test(document.getElementById("Password1").value))
                {
                    if(document.getElementById("Password1").value == document.getElementById("Password2").value)
                    {
                        //console.log(Var_Valid_Pwd);
                        document.getElementById("Label_Valid_Pwd").innerText = Var_Valid_Pwd;
                        //document.getElementById("Submit_btn").disabled = false;
                        return true;
                    }
                    else
                    {
                        //console.log(Var_Invalid_Pwd2);
                        document.getElementById("Label_Valid_Pwd").innerText = Var_Invalid_Pwd2;
                        return false;
                    }
                }
                else
                {
                    document.getElementById("Label_Valid_Pwd").innerText = Var_Invalid_Pwd;
                    //console.log("La contraseña no es valida");
                    return false;
                }

                return false;
            }
            function ValidateUserName()
            {
                if(valid_name.test(document.getElementById("username_form").value))
                {
                    //
                    //console.log("Usuario valido");
                    document.getElementById("Label_Valid_Username").innerText = "";
                    return true;
                }
                else
                {
                    //console.log(Var_Invalid_Username);
                    document.getElementById("Label_Valid_Username").innerText = Var_Invalid_Username;
                    return false;
                }
            }

            function ValidateForm()
            {
                if(ValidateUserName() && ValidatePasswords())
                {
                    document.getElementById("Submit_btn").disabled = false;
                }
                else
                {
                    document.getElementById("Submit_btn").disabled = true;
                }

            }

        </script>
        @include('tema.secciones.scripts')
    </body>

</html>
