@extends('base')

@section('scripts')
<script src="{!! asset('assets/pages/admin/crear.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
<div class="col-lg-12">
	@include('includes.form-error')
	@include('includes.mensaje')
    <div class="card-box">
        <div class="card-block">
        	<a href="{{ route('permiso') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-undo"></i> Volver al listado</a>
            <h5 class="text-bold card-title">Editar Permiso {{$data->nombre}} </h5>
			<div>
				<form action="{{ route('actualizar_permiso', ['id' => $data->id]) }}" id="form-general" method="POST" autocomplete="off">
				@csrf @method('put')
				@include('admin.permiso.form')
                <div class=" text-center">
                	@include('includes.boton-editar-form')
                </div>
            </form>
			</div>
        </div>
    </div>
</div>
@endsection