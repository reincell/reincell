@extends('base')

@section('scripts')
<script src="{!! asset('assets/pages/admin/index.js') !!} " type="text/javascript"></script>
@endsection

@section('contenido')
<div class="col-lg-12">
	@include('includes.mensaje')
    <div class="card-box">
        <div class="card-block">
        	<a href="{{ route('crear_permiso') }}" class="btn btn-primary btn-rounded float-right"><i class="fa fa-plus"></i> Crear Permiso</a>
            <h5 class="text-bold card-title">Permisos</h5>
			<div class="table-responsive">
				<table class="table table-hover col-lg-12" id="tabla-data">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nombre</th>
							<th>Slug</th>
							<th class="width70"></th>
						</tr>
					</thead>
					<tbody>
						@foreach($permisos as $permiso)
							<tr>
								<td>{{ $permiso->id }}</td>
								<td>{{ $permiso->nombre }}</td>
								<td>{{ $permiso->slug }}</td>
								<td>
									<a href="{{route('editar_permiso', ['id' => $permiso->id])}} " class="btn-accion-tabla tooltipsC" title="Editar este registro">
										<i class="fa fa-edit"></i>
									</a>
									<form action="{{route('eliminar_permiso', ['id' => $permiso->id])}}" class="d-inline form-eliminar" method="POST">
										@csrf @method('delete')
										<button type="submit" class="btn-accion-tabla eliminar tooltipsC" title="Eliminar este registro"> <i class="text-danger fa fa-trash"></i> </button>
									</form>
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
        </div>
    </div>
</div>
@endsection