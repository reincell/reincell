	
    <script src="{!! asset('assets/js/jquery-3.2.1.min.js') !!}"></script>
	<script src="{!! asset('assets/js/popper.min.js') !!}"></script>
    <script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.slimscroll.js') !!}"></script>
    <script src="{!! asset('assets/js/Chart.bundle.js') !!}"></script>
    <!--script src="{!! asset('assets/js/chart.js') !!}"></script-->
    <script src="{!! asset('assets/js/app.js') !!}"></script>

    <script src="{!! asset('js/jquery-validation/jquery.validate.min.js') !!}"></script>
    <script src="{!! asset('js/jquery-validation/localization/messages_es.min.js') !!}"></script>
    
    <script src="{!! asset('js/sweet-alert/sweetalert.min.js') !!}"></script>
    <script src="{!! asset('js/toastr/toastr.min.js') !!}"></script>

    <script src="{!! asset('js/scripts.js') !!}"></script>
    <script src="{!! asset('js/jquery-validation/funciones.js') !!}"></script>
    @yield("scriptsPlugins")
    @yield("scripts")