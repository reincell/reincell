@if($item["submenu"] == [])
	<li class="{{getMenuActivo($item["url"])}} ">
		<a href=" {{url($item['url'])}} ">
			<i class="fa fa-{{$item['icono']}} "></i><span>{{$item["nombre"]}} </span>
		</a>
	</li>
@else
	<li class="submenu">
		<a href="#">
			<i class="fa fa-{{$item["icono"]}} "></i><span>{{$item["nombre"]}} </span>
			<span class="menu-arrow"></span>
		</a>
		<ul style="display: none;">
			@foreach($item["submenu"] as $submenu)
				@include('tema.secciones.menu-item', ['item' => $submenu])
			@endforeach
		</ul>
	</li>
@endif