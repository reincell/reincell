<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    
    <link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}">
    <!--link rel="stylesheet" href="{!! asset('assets/css/font-awesome.min.css') !!}"-->
    <link rel="stylesheet" href="{!! asset('css/fuentes-all.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/css/style.css') !!}">
    @yield("styles")
    <link rel="stylesheet" href="{!! asset('js/toastr/toastr.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
    <title>Administración Reincell</title>
</head>