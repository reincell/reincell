
<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul class="sidebar-menu">
                <li class="menu-title">Menú Principal</li>
                @foreach($menusComposer as $key => $item)
                    @if($item['menu_id'] != 0)
                        @break
                    @endif
                    @include('tema.secciones.menu-item', ['item' => $item])
                @endforeach                        
            </ul>
        </div>
    </div>
</div>