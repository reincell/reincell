<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }
}
