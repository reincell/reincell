<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    protected $table = "roles";
    protected $fillable = ['name', 'description'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
}
