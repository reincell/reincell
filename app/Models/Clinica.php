<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    protected $table = "clinica";
    protected $fillable = ['nombre', 'telefono', 'tipo', 'domicilio', 'colonia', 'ciudad', 'pais'];
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
}
