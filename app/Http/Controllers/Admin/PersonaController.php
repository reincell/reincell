<?php

namespace App\Http\Controllers\Admin;

use App\Persona;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
//use App\Http\Controllers\Auth;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.actualizar_datos');
    }

    public function perfil()
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $persona = $user->persona()->get()->first();
        $new_user = $user->leftJoin('personas', 'personas.user_id', '=', 'users.id')->select('users.*', 'personas.*')->where('users.id', '=', $user->id)->get()->first();

        //return $new_user;
        return view('admin.profile')->with('persona', $new_user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function show(persona $persona)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function edit(persona $persona)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, persona $persona)
    {
        //
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $persona = $user->persona()->get()->first();

        $persona->nombres = $request->nombres;
        $persona->apellido_paterno = $request->apellido_paterno;
        $persona->apellido_materno = $request->apellido_materno;
        $persona->curp = $request->curp;
        $persona->telefono = $request->telefono;
        $persona->direccion = $request->direccion;
        $persona->pais = $request->pais;
        $persona->estado = $request->estado;
        $persona->ciudad = $request->ciudad;
        $persona->colonia = $request->colonia;
        $persona->codigo_postal = $request->codigo_postal;

        if($persona->save())
        {
            $mensaje = "Exito has actualizado tus datos";
        }
        else {
            $mensaje = "Ha ocurrido un error inesperado";
        }

        return redirect('admin/perfil')->with('mensaje', $mensaje);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function destroy(persona $persona)
    {
        //
    }
}
