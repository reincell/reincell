<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ValidacionUsuarios;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\Persona;
use Naif\CpanelMail\Facades\CpanelMail;
use Naif\cPanelMail\cPanel;
use Naif\cPanelMail\cPanelBaseModule;
use Naif\cPanelMail\cPanelEmail;
use Naif\cPanelMail\cPanelServiceProvider;
use Naif\cPanelMail\Email;

class UserRegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.registro_usuario');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidacionUsuarios $request)
    {
        //
        //UserRegister::create($request->all());
        $cpanel = new cPanel();
        $response = $cpanel->getEmailAddresses();

        $emails_registered = array();

        foreach ($response as $email_data)
        {
            array_push($emails_registered, $email_data->email);
            //echo $email_data->email."/br"; // this is your area from json response
        }
        //print_r($emails_registered);

        //echo $response;
        if (in_array($request->name."@reincell.com", $emails_registered))
        {
            // $Exist= "Existe: ".$request->name."@reincell.com";
            //$mensaje = 'Usuario existente!';
        }
        else
        {

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->name."@reincell.com";
            $user->password = Hash::make($request->password);
            $user->save();

            $persona = new Persona();
            $persona->nombres = "Miguel";
            $persona->user_id = User::where('email', $user->email)->first()->id;
            $persona->save();

            /*
            $user = User::create([
                'name' => $request->name,
                'email' => $request->name."@reincell.com",
                'password' => Hash::make($request->password),
            ]);




            Persona::create([
                'nombre' => 'Miguel',
            ]);
            */
            $user->roles()->attach(Role::where('id', $request->rol)->first());
            //$user->persona()->attach(Persona::where('id', 1)->first());

            //$cpanel->create($request->name, $request->password);
            //$cpanel->changeQuota($request->name, 100);
            //quota as a number or 0 to set it as unlimited

            //$Exist = "El correo es valido";
            $mensaje = 'Usuario creado con Exito!';
        }
        //echo Role::find($request->rol);
        //echo $request->rol;
        return redirect('admin/email')->with('mensaje', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserRegister  $userRegister
     * @return \Illuminate\Http\Response
     */
    public function show(UserRegister $userRegister)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserRegister  $userRegister
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRegister $userRegister)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserRegister  $userRegister
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserRegister $userRegister)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserRegister  $userRegister
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRegister $userRegister)
    {
        //
    }
}
