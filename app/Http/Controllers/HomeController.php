<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {

        /*if(($request->user()->myRole() == '8')){
            $request->user()->authorizeRoles(['user', 'empleado']);
            return view('index');
        }
        else if(($request->user()->myRole() == '3')){
            $request->user()->authorizeRoles(['user', 'admin']);
            return view('index');
        }
        */
        /*$request->user()->authorizeRoles(['user', 'admin']);
        
        return view('admin');
        */
        return view('index');
    }
}
