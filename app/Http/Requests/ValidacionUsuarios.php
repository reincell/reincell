<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidacionUsuarios extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50|unique:users,name'. $this->route('id'),
            'password' => 'nullable|max:50',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El campo nombre es requerido',
            'name.max' => 'El campo nombre no debe ser mayor a 50 caracteres',
            'name.unique' => 'El campo nombre actualmente esta registrado',
            'password.required' => 'Contraseña es requerida',
            'password.max' => 'Contraseña no debe ser mayor a 50 caracteres',
        ];
    }
}
