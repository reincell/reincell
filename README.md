**Reincell**

Esta aplicación se encuentra en desarrollo

Para lograr ejecutar esta aplicación es necesario realizar los siguientes pasos:

1.  Clonar el repositorio en una carpeta local.
2.  Con la consola posicionarse dentro de la carpeta obtenida y ejecutar el siguiente comando: (**composer install**).
3.  Crear un **.env** con el siguiente comando: (**copy .env.example .env**).
4.  Crear una llave de laravel con el siguiente comando: (**php artisan key:generate**).
5.  La base de datos debe tener el cotejamiento: (**utf8mb4_spanish_ci**).
5.  Modificar el archivo **.env** y colocar los datos relacionados a la base de datos local y todas las variables necesarias (*pedir el **.env** a un administrador*).
6.  Ejecutar todas las migraciones y las semillas con el comando: (**php artisan migrate:fresh --seed**).
7.  Descargar un certificado ssl para el proyecto que se puede encontrar aquí: ([Certificado](http://curl.haxx.se/ca/cacert.pem)).
8.  Pegar el certificado en una carpeta de nuestro servidor local y guardar la ruta donde se guardó, ejemplo: (**"C:\wamp64\certificado\cacert.pem"**)
9.  Ahora ejecutaremos en la consola el siguiente comando: (**php --ini**)
10. Este comando nos indicará la ruta exacta del archivo que debemos modificar, por ejemplo: (**"C:\wamp64\bin\php\php7.3.5\php.ini"**).
11. Abriremos con el WAMP o XAMP apagado el archivo **php.ini** y lo editaremos.
10. Si bajamos completamente en el documento encontraremos esta línea de código (**;curl.cainfo =**) el ";" al inicio indica que esta comentada.
11. La descomentaremos para cambiarlo por: (**curl.cainfo = "C:\wamp64\certificado\cacert.pem"**), este es un ejemplo teniendo en cuenta que anteriormente descargamos el certificado.
12. Abremos cambiado la línea de código de esta manera: (**;curl.cainfo =**) -> (**curl.cainfo = "C:\wamp64\certificado\cacert.pem"**), sin los paréntesis.
13. Reiniciaremos el servidor WAMP o XAMP.
12. Con esto se podrá acceder a la API cPanel para funcionar correctamente.